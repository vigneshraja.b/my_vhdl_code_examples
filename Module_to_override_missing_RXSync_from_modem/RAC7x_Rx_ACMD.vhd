---------------------------------------------------------------------------------
-- COPYRIGHT (C) 2016 Aviat Networks.
-- ALL RIGHTS RESERVED
--------------------------------------------------------------------------------
-- Author  	   : Vignesh Balu
-- File Name   : RAC7x_Rx_ACMD.vhd
-- Function    : Eclipse
-- Description : Derived from RAC6x Rx_ACM_Decoder module.
--               The BCM650 interface is somewhat different to its predecessor PVG620.
--               This module monitors the GPI_RACM input w.r.t. REN on using internal 100MHz system clock
--               BCM650 Inserts Serial GPI RACM with a Start bit, followed by 4 bit consecutive profile ID.
--               This is inserted 15~16 clock read enable cycles ahead of LDPC frame Sync!.
--				 some times the RXsync is missed, State machine added with a condition to mask this issue from the modem.

-- History:
-- Date        Engineer        Description
-- 19/10/2016  JKC             Created

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;


entity RAC7x_Rx_ACMD is
port (
    Reset           : in  std_logic;                        -- Active high asynchronous reset
    Rx_Clk_Enbl     : in  std_logic;                        -- H = enable, same as Demux
    Clock           : in  std_logic;                        -- 100MHz
    Clk_en          : in  std_logic;                        -- 100MHz clock enable
    RxACM_Ser       : in  std_logic;                        -- Rx ACM Serial data in from BCM650, at Clk Enable
    RxSync          : in  std_logic;                        -- Rx LDPC Frame Sync from BCM650
    DmxSFSync       : in  std_logic;                        -- Dmx LDPC Frame Sync from Deframer, for hitless override
    Loopback_Enable : in  std_logic;                        --
    ACMOverride     : in  std_logic_vector(07 downto 00);   -- From uP interface, override waht PVG sends
    RxACM_Control   : out std_logic_vector(03 downto 00);   -- ACM value after the frame delays ie. prenote delay
    RxACM_Strobe    : out std_logic;                        -- Strobe alligned with the ACM;
    RxACMChanged    : out std_logic;                         -- Pulse at ACM value change, for use as trigget TP
    State_Tps       : out std_logic_vector(14 downto 0)    -- TPs to use in logic analyser
);
end RAC7x_Rx_ACMD;

Architecture BHV of RAC7x_Rx_ACMD is

type FSM_S is (START, RDY, LATCH, IDLE);
signal  FSM_RACM : FSM_S;

signal ACM_i              :   std_logic_vector( 3 downto 0);
signal ACM_L              :   std_logic_vector( 3 downto 0);
signal ACM_S              :   std_logic_vector( 3 downto 0);
signal cnt4_slv           :   std_logic_vector(2 downto 0);

Begin

A: Process(Reset, Clock, clk_en)
variable cnt4: integer;
Begin
   if Reset = '1' then
      FSM_RACM <= IDLE;
      cnt4 := 0;
      ACM_i           <= (others => '0');
      ACM_S           <= (others => '0');
   elsif Clock'event and Clock = '1' and clk_en = '1' then
      if Rx_Clk_Enbl = '1' then
        case FSM_RACM is
           when IDLE =>
              if RxACM_ser = '1' then  --Start
                 FSM_RACM <= START;
              end if;
           when START =>
              if cnt4 = 3 then
                 FSM_RACM <= RDY;
                 cnt4 := 0;
              else
                 cnt4 := (cnt4 + 1) mod 4;
              end if;
              ACM_i <= RxACM_Ser & ACM_i(3 downto 1);                   -- Shift register with lsb first
           when RDY =>
              --if DmxSFSync = '1' then
              if Rxsync = '1' then
                 ACM_S <= ACM_i;
                 FSM_RACM <= LATCH;
              else
                 if RxACM_ser = '1' then  --Start
                    FSM_RACM <= START;
                 end if;
              end if;
           when LATCH =>
              ACM_i <= (others => '0');
              cnt4 := 0;
              FSM_RACM <= IDLE;
         end case;
      end if;
      cnt4_slv <= std_logic_vector(to_unsigned(cnt4, cnt4_slv'length));
   end if;
end process;

        --Internal Override or during Muldem Loopback

OVERRIDE: process(reset, clock)
begin
   if reset = '1' then
      ACM_L <= (others => '0'); --current;
      RxACMChanged    <= '0';
      RxACM_Strobe  <= '0';
   elsif clock'event and clock = '1' then
      if Loopback_Enable = '1' then                 -- At sync so switch error free in muldem lpbk
         if ACMOverride(7) = '1' AND DmxSFSync = '1' then                 -- At sync so switch error free in muldem lpbk
           ACM_L <= ACMOverride(3 downto 0);   -- From microprocessor interface
         else
           ACM_L <= ACMOverride(3 downto 0);   -- From microprocessor interface
         end if;
      else
        if ACMOverride(7) = '1' then
           ACM_L <= ACMOverride(3 downto 0);   -- From microprocessor interface
        else
           ACM_L <= ACM_S;                     -- Latched from PVG
        end if;
      end if;

      if ACM_L /= ACM_S then
        RxACMChanged    <= '1';
        RxACM_Strobe  <= '1';
      else
        RxACMChanged    <= '0';
        RxACM_Strobe  <= '0';
      end if;
  end if;
end process;

RxACM_Control <= ACM_L;
State_Tps(3 downto 0) <= ACM_L;
State_Tps(7 downto 4) <= ACM_i;
State_Tps(11 downto 8) <= ACM_S;
State_Tps(14 downto 12) <= cnt4_slv;

end BHV;


