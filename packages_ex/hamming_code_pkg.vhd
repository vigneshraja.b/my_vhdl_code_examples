-------------------------------------------------------------------------------
-- Title      : hamming code pkg
-- Project    : 
-------------------------------------------------------------------------------
-- File       : common_types_pkg.vhd
-- Author     : Vignesh raja Balu <v.balu@massey.ac.nz>
-- Company    : Center for Research in Analog and VLSI micro-system Design lab, Massey University
-- Created    : 
-- Last update: 
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: FUnctions to perform hamming encode decode, find code length and data length
-------------------------------------------------------------------------------
-- Copyright (c) 2015 High Performance Computing Research Lab, Auckland University of Technology
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.misc_tools_pkg.all;

package hamming_code_pkg is

    --* Add parity bits to a data vector of arbitrary length.
    --* Use code_length(data'length) function to determine the output code length.    
    function hamming_encode (
        i_data : std_logic_vector)
        return std_logic_vector;

    --* Remove parity bits from a hamming code of arbitrary length.
    --* Will correct for single bit errors in the code.
    --* Use data_length(code'length) function to determine the output data length.
    function hamming_decode (
        code : std_logic_vector)
        return std_logic_vector;

    --* Calculate the length of the hamming code for the given data word length.
    function code_length (
        constant data_length : natural)
        return natural;

    --* Calculate the length of the data word represented by a given code word length.
    function data_length (
        constant code_length : natural)
        return natural;

end package hamming_code_pkg;

package body hamming_code_pkg is

    function code_length (
        constant data_length : natural)
        return natural is
        variable parity : natural;
        variable d_len : natural;            
    begin               
        parity := 2;
        while 2**parity - parity - 1 < data_length loop
            parity := parity + 1;
        end loop;
        return data_length + parity;
    end function code_length;

    function data_length (
        constant code_length : natural)
        return natural is
        variable est : natural;
    begin
        assert code_length >= 3
            report "Hamming code length must be 3 or greater."
            severity failure;
        if code_length <= 4 then
            return 1;
        elsif code_length = 5 then
            return 2;
        elsif code_length <= 7 then
            return code_length - 3;
        end if;
        est := code_length - ceil_log2(code_length);
        if is_pow_2(code_length) then
            est := est - 1;
        end if;
        return est;
    end function data_length;

    function hamming_encode (
        i_data : std_logic_vector)
        return std_logic_vector is
        variable data : std_logic_vector(i_data'length-1 downto 0);
        variable hamming  : std_logic_vector(code_length(data'length) downto 1);
        variable d_idx    : natural;
        variable h_idx_us : unsigned(ceil_log2(hamming'length) downto 0);
    begin
        data := i_data;                 -- ensure a zero indexed vector.
        d_idx := 0;
        -- First put the data in the right places.
        -- Power of 2 positions are reserved for parity bits.
        -- Note that the indexing starts at 1 in the hamming variable.
        for idx in hamming'range loop
            if is_pow_2(idx) then
                hamming(idx) := '0';
            else
                hamming(idx) := data(d_idx);
                d_idx        := d_idx + 1;
            end if;
        end loop;
        -- Calculate the parity bits.
        for p_idx in hamming'range loop
            if is_pow_2(p_idx) then                -- is a parity bit.
                for h_idx in p_idx to hamming'high loop
                    h_idx_us := to_unsigned(h_idx, h_idx_us'length);
                    if h_idx_us(ceil_log2(p_idx)) = '1' then  -- this bit belongs to the parity.
                        hamming(p_idx) := hamming(p_idx) xor hamming(h_idx);
                    end if;
                end loop;  -- h_idx
            end if;
        end loop;  -- p_idx
        return hamming;
    end function hamming_encode;

    function hamming_decode (
        code : std_logic_vector)
        return std_logic_vector is
        variable data     : std_logic_vector(data_length(code'length)-1 downto 0);
        variable hamming  : std_logic_vector(code'length downto 1);
        variable parity   : unsigned(ceil_log2(code'length)-1 downto 0);
        variable d_idx    : natural;
        variable h_idx_us : unsigned(ceil_log2(hamming'length+1)-1 downto 0);
    begin
        hamming := code;                       -- shifts the indexing to start at 1.
        parity := to_unsigned(0, parity'length);
        -- check the parity bits.
        for p_idx in parity'range loop
            for h_idx in hamming'range loop
                h_idx_us := to_unsigned(h_idx, h_idx_us'length);
                if h_idx_us(p_idx) = '1' then  -- this bit of the hamming code belongs to p_idx parity bit.
                    parity(p_idx) := parity(p_idx) xor hamming(h_idx);
                end if;
            end loop;
        end loop;
        -- the value of the parity variable specifies the bit position in error.
        -- Correct it.
        for h_idx in hamming'range loop
            if parity = h_idx then
                hamming(h_idx) := not hamming(h_idx);
                --report "error corrected at bit" & to_string(to_integer(parity)) severity note;
            end if;
        end loop;
        -- extract the data from the hamming word.
        d_idx := 0;
        -- Power of 2 positions are reserved for parity bits.
        for h_idx in hamming'range loop
            if not is_pow_2(h_idx) then
                data(d_idx) := hamming(h_idx);
                d_idx       := d_idx + 1;
            end if;
        end loop;
        return data;
    end function hamming_decode;

end package body hamming_code_pkg;

