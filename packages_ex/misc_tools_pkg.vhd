-------------------------------------------------------------------------------
-- Title      : Misc_tools
-- Project    : 
-------------------------------------------------------------------------------
-- File       : common_types_pkg.vhd
-- Author     : Vignesh raja Balu <v.balu@massey.ac.nz>
-- Company    : Center for Research in Analog and VLSI micro-system Design lab, Massey University
-- Created    : 
-- Last update: 
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Some usefull DSP functions 
-------------------------------------------------------------------------------
-- Copyright (c) 2015 High Performance Computing Research Lab, Auckland University of Technology
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.common_types_pkg.all;

package misc_tools_pkg is

    function wired_or (slv  : std_logic_vector) return std_logic;
    function wired_and (slv : std_logic_vector) return std_logic;
    function shift_left (slv             : std_logic_vector;
                         constant amount : natural   := 1;
                         constant fill   : std_logic := '-') return std_logic_vector;
    function shift_right (slv             : std_logic_vector;
                          constant amount : natural   := 1;
                          constant fill   : std_logic := '-') return std_logic_vector;

    -- floor(log_b(n))
    function floor_log(val, base : positive)
        return natural;

    -- ceiling(log_b(n))
    function ceil_log(val, base : positive)
        return natural;

    -- floor(log2(n))
    function floor_log2(val : positive)
        return natural;

    -- ceiling(log2(n))
    function ceil_log2(val : positive)
        return natural;

    -- Number of bits needed to represent a number n in binary
    function bit_width(val : natural)
        return natural;

    -- Number of bits needed to encode n items
    function encoding_width(val : positive)
        return natural;

    function power_of_2 (power        : unsigned;
                         constant len : natural)
        return unsigned;

    -- Returns the next power of two greater than or equal val
    function ceil_pow2 (val : natural)
        return natural;

    function is_pow_2 (val : natural)
        return boolean;

    function maximum (constant a : natural;
                      constant b : natural)
        return natural;

    function bit_swap (slv                  : std_logic_vector;
                       constant slice_width : natural := 1)  -- size of the groups to swap. e.g swap order of bytes = 8.
        return std_logic_vector;

    function byte_swap(slv : std_logic_vector)
        return std_logic_vector;

  
end package misc_tools_pkg;

package body misc_tools_pkg is

    -- purpose: OR all bits together
    function wired_or (slv : std_logic_vector)
        return std_logic is
        variable ored : std_logic := '0';
    begin  -- function wired_or
        if slv = (slv'range => '0') then
            return '0';
        else
            return '1';
        end if;
    end function wired_or;

    function wired_and (slv : std_logic_vector)
        return std_logic is
        variable ored : std_logic := '0';
    begin  -- function wired_or
        if slv = (slv'range => '1') then
            return '1';
        else
            return '0';
        end if;
    end function wired_and;

    -- purpose: shift a std_logic_vector left an <amount> and shift in bits with value <fill>.
    function shift_left (slv             : std_logic_vector;
                         constant amount : natural   := 1;
                         constant fill   : std_logic := '-')
        return std_logic_vector is
    begin
        return slv(slv'high-amount downto slv'low) & (amount-1 downto 0 => fill);
    end function shift_left;

    function shift_right (slv             : std_logic_vector;
                          constant amount : natural   := 1;
                          constant fill   : std_logic := '-')
        return std_logic_vector is
    begin
        return (amount-1 downto 0 => fill) & slv(slv'high downto slv'low+amount);
    end function shift_right;

    function floor_log(val, base : positive)
        return natural is
        variable log, residual : natural;
    begin
        residual := val;
        log      := 0;
        while residual > (base - 1) loop
            residual := residual / base;
            log      := log + 1;
        end loop;
        return log;
    end function;

    function ceil_log(val, base : positive)
        return natural is
        variable log, residual : natural;
    begin
        residual := val - 1;
        log      := 0;
        while residual > 0 loop
            residual := residual / base;
            log      := log + 1;
        end loop;
        return log;
    end function;

    function floor_log2(val : positive)
        return natural is
    begin
        return floor_log(val, 2);
    end function;

    function ceil_log2(val : positive)
        return natural is
    begin
        return ceil_log(val, 2);
    end function;

    function bit_width(val : natural)
        return natural is
    begin
        if val = 0 then
            return 1;
        else
            return floor_log2(val) + 1;
        end if;
    end function;

    function encoding_width(val : positive)
        return natural is
    begin
        if val = 1 then
            return 1;
        else
            return ceil_log2(val);
        end if;
    end function;

    function power_of_2 (power        : unsigned;
                         constant len : natural)
        return unsigned is
        variable res : unsigned(len -1 downto 0) := (others => '0');
    begin
        res(to_integer(power)) := '1';
        return res;
    end function power_of_2;

    -- Returns the next power of two.
    function ceil_pow2 (val : natural)
        return natural is
        variable res : natural;
    begin  -- function ceil_pow2
        if val = 0 then
            return 0;
        end if;
        res := 1;
        while res < val loop
            res := res * 2;
        end loop;
        return res;
    end function ceil_pow2;

    function is_pow_2 (
        val : natural)
        return boolean is
        variable test : natural;
    begin  -- function is_pow_2
        test := val;
        while test mod 2 = 0 loop
            test := test/2;
        end loop;
        return test = 1;
    end function is_pow_2;

    function maximum (constant a : natural;
                      constant b : natural)
        return natural is
    begin  -- function maximum
        if a > b then
            return a;
        else
            return b;
        end if;
    end function maximum;

    function bit_swap (slv                  : std_logic_vector;
                       constant slice_width : natural := 1)  -- size of the groups to swap. e.g swap order of bytes = 8.
        return std_logic_vector is
        variable out_slv : std_logic_vector(slv'range);
        variable lo, hi  : natural;
    begin
        assert slv'length mod slice_width = 0
            report "std_logic_vector input length (" & natural'image(slv'length) & ") is not a multiple of the slice_width (" & natural'image(slice_width) & ")."
            severity failure;
        for idx in 0 to slv'length/slice_width - 1 loop
            lo                                  := out_slv'low + idx * slice_width;
            hi                                  := slv'high - idx * slice_width;
            out_slv(lo+slice_width-1 downto lo) := slv(hi downto hi-slice_width+1);
        end loop;
        return out_slv;
    end function bit_swap;

    function byte_swap(slv : std_logic_vector)
        return std_logic_vector is
    begin
        return bit_swap(slv, 8);
    end function byte_swap;

end package body misc_tools_pkg;

