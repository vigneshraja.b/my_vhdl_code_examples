-------------------------------------------------------------------------------
-- Title      : Common Type Declarations
-- Project    : 
-------------------------------------------------------------------------------
-- File       : common_types_pkg.vhd
-- Author     : Vignesh raja Balu <v.balu@massey.ac.nz>
-- Company    : Center for Research in Analog and VLSI micro-system Design lab, Massey University
-- Created    : 
-- Last update: 
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: std_logic_vector arrays of size 2^n
-------------------------------------------------------------------------------
-- Copyright (c) 2015 High Performance Computing Research Lab, Auckland University of Technology
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package common_types_pkg is

    type slv_1_a is array (natural range <>) of std_logic_vector(0 downto 0);
    type slv_2_a is array (natural range <>) of std_logic_vector(1 downto 0);
    type slv_4_a is array (natural range <>) of std_logic_vector(3 downto 0);    
    type slv_8_a is array (natural range <>) of std_logic_vector(7 downto 0);
    type slv_16_a is array (natural range <>) of std_logic_vector(15 downto 0);
    type slv_32_a is array (natural range <>) of std_logic_vector(31 downto 0);
    type slv_64_a is array (natural range <>) of std_logic_vector(63 downto 0);
    type slv_128_a is array (natural range <>) of std_logic_vector(127 downto 0);
    type slv_256_a is array (natural range <>) of std_logic_vector(255 downto 0);

    type unsigned_1_a is array (natural range <>) of unsigned(0 downto 0);
    type unsigned_2_a is array (natural range <>) of unsigned(1 downto 0);
    type unsigned_4_a is array (natural range <>) of unsigned(3 downto 0);    
    type unsigned_8_a is array (natural range <>) of unsigned(7 downto 0);
    type unsigned_16_a is array (natural range <>) of unsigned(15 downto 0);
    type unsigned_32_a is array (natural range <>) of unsigned(31 downto 0);
    type unsigned_64_a is array (natural range <>) of unsigned(63 downto 0);
    type unsigned_128_a is array (natural range <>) of unsigned(127 downto 0);
    type unsigned_256_a is array (natural range <>) of unsigned(255 downto 0);
    
     
end package common_types_pkg;

