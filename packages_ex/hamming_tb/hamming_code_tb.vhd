-------------------------------------------------------------------------------
-- Title      : hamming code test
-- Project    : 
-------------------------------------------------------------------------------
-- File       : common_types_pkg.vhd
-- Author     : Vignesh raja Balu <v.balu@massey.ac.nz>
-- Company    : Center for Research in Analog and VLSI micro-system Design lab, Massey University
-- Created    : 
-- Last update: 
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Test bench to test the hamming code functions in hamming_code_pkg.
-- encoder generates the code (no error)
-- signal salt is used to introduce bit error in output of encoder.
-- decoder identifies and correct it. 
-------------------------------------------------------------------------------
-- Copyright (c) 2015 High Performance Computing Research Lab, Auckland University of Technology
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.hamming_code_pkg.all;

entity hamming_code_tb is
    generic (
        g_DATA_WIDTH : natural := 5);

end entity hamming_code_tb;

architecture sim of hamming_code_tb is
    signal data_in  : unsigned(g_DATA_WIDTH-1 downto 0)                        := (others => '0');
    signal code     : std_logic_vector(code_length(data_in'length)-1 downto 0) := (others => '0');
    signal code_err : std_logic_vector(code_length(data_in'length)-1 downto 0) := (others => '0');
    signal salt     : std_logic_vector(code'range)                             := (others => '0');
    signal data_out : unsigned(data_length(code'length)-1 downto 0)            := (others => '0');

    signal i_clk : std_logic := '1';
begin  -- architecture sim

    i_clk <= not i_clk after 5 ns;

    P_TEST : process is

    begin
        data_in <= to_unsigned(0, data_in'length);
        while true loop
            for err_bit in salt'range loop
                salt          <= (others => '0');
                salt(err_bit) <= '1';
                wait until rising_edge(i_clk);
            end loop;
            data_in <= data_in + 1;
        end loop;
    end process;

    code     <= hamming_encode(std_logic_vector(data_in));
    code_err <= code xor salt;
    data_out <= unsigned(hamming_decode(code_err));

    P_CHECK : process (data_out) is
    begin
        assert data_in = data_out report "Data out has an uncorrected error.";
    end process;


end architecture sim;

