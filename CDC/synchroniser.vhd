-------------------------------------------------------------------------------
-- Title      : Synchroniser flops
-- Project    : 
-------------------------------------------------------------------------------
-- File       : synchroniser.vhd
-- Author     : Vignesh raja Balu <v.balu@massey.ac.nz>
-- Company    : Center for Research in Analog and VLSI micro-system Design lab, Massey University
-- Created    : 
-- Last update: 
-- Platform   : 
-- Standard   : 
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2018 High Performance Computing Research Lab, Auckland University of Technology
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity synchroniser is

    generic (
        g_SYNCHRONISER_FLOPS : natural range 2 to 4 := 3);

    port (
        i_bit    : in  std_logic;       -- bit in another clock domain.
        i_clk    : in  std_logic;
        o_bit_rt : out std_logic);      -- synchronised to i_clk domain.

end entity synchroniser;

architecture rtl of synchroniser is

    signal synchroniser_in_false_path_anchor : std_logic;
    signal pipe : std_logic_vector(g_SYNCHRONISER_FLOPS -2 downto 0);

begin  -- architecture rtl

    P_FLOPS : process (i_clk) is
    begin
        if rising_edge(i_clk) then
            synchroniser_in_false_path_anchor <= i_bit;
            pipe <= synchroniser_in_false_path_anchor & pipe(pipe'high downto 1);
        end if;
    end process;

    o_bit_rt <= pipe(0);

end architecture rtl;

