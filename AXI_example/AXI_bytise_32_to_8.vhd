-------------------------------------------------------------------------------
-- Title      : AXI_bytise_32_to_8
-- Project    : CMAC
-------------------------------------------------------------------------------
-- File       : AXI_bytise_32_to_8.vhd
-- Author     : Vignesh raja Balu <v.balu@massey.ac.nz>
-- Company    : Center for Research in Analog and VLSI micro-system Design lab, Massey University
-- Created    : 
-- Last update: 
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: This is to covert 32 bit AXI4 data to 4x8 bit AXI4 data
-- 
-- AXI4_pkg includes type AXI4_streaming_32_t, and AXI4_Streaming_8_t
--  type AXI4_Streaming_32_t is record
--      data  : std_logic_vector(31 downto 0);
--      valid : std_logic;
--      last  : std_logic;
--      keep  : std_logic_vector(3 downto 0);
--      error : std_logic;
--  end record AXI4_Streaming_32_t;
--  type AXI4_Streaming_8_t is record
--         data  : std_logic_vector(7 downto 0);
--         valid : std_logic;
--         last  : std_logic;
--         keep  : std_logic_vector(0 downto 0);
--         error : std_logic;
--     end record AXI4_Streaming_8_t;
-------------------------------------------------------------------------------
-- Copyright (c) 2015 High Performance Computing Research Lab, Auckland University of Technology
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.misc_tools_pkg.all;
use work.AXI4_pkg.all;

entity AXI_bytise_32_to_8 is
    
    port (i_clk       : in  std_logic;
          i_clk_reset : in  std_logic;
          i_stream    : in  AXI4_Streaming_32_t;
          o_ready     : out std_logic;
          o_stream    : out AXI4_Streaming_8_t;
          i_stop4x    : in  std_logic
          );

end entity AXI_bytise_32_to_8;

architecture rtl of AXI_bytise_32_to_8 is

    signal byte_cnt : unsigned(1 downto 0) := (others => '0');
    signal internal : AXI4_Streaming_32_t;
    
begin  -- architecture rtl

    o_ready <= (not i_stop4x) when byte_cnt = 0 else '0';

    P_BYTISE : process (i_clk) is
    begin  -- process
        if rising_edge(i_clk) then      -- rising clock edge
            if i_clk_reset = '1' then   -- synchronous reset (active high)
                byte_cnt <= to_unsigned(0, byte_cnt'length);
            else
                if byte_cnt > 0 or (i_stream.valid = '1' and i_stop4x = '0') then
                    byte_cnt <= byte_cnt - 1;
                end if;
            end if;

            if byte_cnt = 0 then
                if i_stop4x = '1' then
                    internal.valid <= '0';
                else
                    internal <= i_stream;
                end if;
            else
                internal.data <= shift_left(internal.data, o_stream.data'length);
                internal.keep <= shift_left(internal.keep, o_stream.keep'length, '0');
            end if;

            o_stream.data  <= internal.data(internal.data'high downto internal.data'length - o_stream.data'length);
            o_stream.keep  <= internal.keep(internal.keep'high downto internal.keep'length - o_stream.keep'length);
            o_stream.error <= internal.error;

            if i_clk_reset = '1' then
                o_stream.valid <= '0';
            else
                o_stream.valid <= internal.valid and wired_or(internal.keep(internal.keep'high downto internal.keep'length - o_stream.keep'length));
            end if;

            if internal.keep = "1000" then
                o_stream.last <= internal.last;
            else
                o_stream.last <= '0';
            end if;
        end if;
    end process;



end architecture rtl;
