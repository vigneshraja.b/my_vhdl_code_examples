-------------------------------------------------------------------------------
-- Title      : Test harness for design "AXI_wordise"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : AXI_wordise_pak_gen_th.vhd
-- Author     : Vignesh raja Balu <v.balu@massey.ac.nz>
-- Company    : Center for Research in Analog and VLSI micro-system Design lab, Massey University
-- Created    : 2018-07-24
-- Last update: 2018-07-28
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2018 Center for Research in Analog and VLSI micro-system Design lab, Massey University
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;
 
library osvvm;
use osvvm.RandomPkg.all;
 
library work;
use work.AXI4_pkg.all;
 
------------------------------------------------------------------------------------------------------------------------
 
entity AXI_wordise_tb is
    generic(
        runner_cfg : string
        );
end entity AXI_wordise_tb;
 
------------------------------------------------------------------------------------------------------------------------
 
architecture sim of AXI_wordise_tb is
    constant c_PUBLISHER_NAME   : string  := "trans_source";
    constant c_NUM_PKTS         : natural := 200;
    constant c_OUT_SKIP         : natural := 1;
    constant c_RDM_PKT_WAIT     : natural := 7;
    
    constant AXI4_Streaming_32_T_dontcare : AXI4_Streaming_32_t := (
        data    => (others =>'-'),
        valid   => '-',
        last    => '-',
        keep    => (others => '-'),
        error   => '-'
        );
 
    -- component ports
    signal i_clk       : std_logic := '1';
    signal i_clk_reset : std_logic;
    signal i_stream    : AXI4_Streaming_8_t := AXI4_STREAMING_8_T_ZERO ;
    signal o_stop      : std_logic;
    signal o_stream    : AXI4_Streaming_32_t;
    signal i_stop      : std_logic;
 
    -- component checker
    
    signal chk_buffer_in_valid  : std_logic;
    signal chk_buffer_in        : AXI4_Streaming_8_t;  
    signal chk_buffer_out       : AXI4_Streaming_32_t := AXI4_STREAMING_32_T_ZERO;
    signal chk_dut_out          : AXI4_Streaming_32_t := AXI4_STREAMING_32_T_ZERO;
 
    signal chk_buffer           : AXI4_Streaming_8_t_a (0 to 7);

    signal eof_pkt_gen          :std_logic := '0'; -- end of packet generation
 
begin  -- architecture sim
 
    -- clock generation
    i_clk       <= not i_clk after 10 ns;
    i_clk_reset <= '1', '0'  after 100 ns;
 
    -- packet generation    
    P_MAIN : process
        variable rand_pkt_gap   : boolean;
        variable rand_valid     : boolean;        
        variable self           : actor_t := new_actor(c_PUBLISHER_NAME);
        variable message        : msg_t; 
        variable reply_msg      : msg_t; 
    begin
        test_runner_setup(runner,runner_cfg);
        set_stop_level(failure);
        wait until i_clk_reset = '0';
        wait until rising_edge(i_clk);
        while (test_suite) loop
            if run("AXI_wordise_continuous_pkt_gen") then
                rand_pkt_gap    := false;
                rand_valid      := false;
                message         := new_msg;
                push(message, rand_pkt_gap);
                push(message, rand_valid);
                send(net, self, message);
            elsif run("AXI_wordise_rand_pkt_gap") then
                rand_pkt_gap    := true;
                rand_valid      := false;
                message         := new_msg;
                push(message, rand_pkt_gap);
                push(message, rand_valid);
                send(net, self, message);
            elsif run("AXI_wordise_random_valid") then
                rand_pkt_gap    := false;
                rand_valid      := true;
                message         := new_msg;
                push(message, rand_pkt_gap);
                push(message, rand_valid);
                send(net, self, message);
             end if;
        end loop;
        receive_reply(net, message, reply_msg);
        eof_pkt_gen <= pop(reply_msg);
        if eof_pkt_gen ='1' then
            report integer'image(c_NUM_PKTS)& "Packets have been checked";
        end if;
        delete(reply_msg);
        test_runner_cleanup(runner);
        wait;
    end process;
    
    E_PKT_GEN: entity work.AXI_wordise_pkt_gen_th
       generic map (
            g_PUBLISHER_NAME    => c_PUBLISHER_NAME,
            g_NUM_PKTS          => c_NUM_PKTS,
            g_RDM_PKT_WAIT      => c_RDM_PKT_WAIT)
        port map(
            i_clk               => i_clk,
            i_clk_reset         => i_clk_reset,             
            o_stream_req        => i_stream);

    -- component instantiation
    DUT : entity work.AXI_wordise_8_to_32
        generic map (
            g_OUT_SKIP => c_OUT_SKIP)  -- [natural] indexed from the right.
        port map (
            i_clk       => i_clk,               -- [std_logic]
            i_clk_reset => i_clk_reset,         -- [std_logic]
            i_stream    => i_stream,            -- [AXI4_Streaming_8_t]
            o_stop      => o_stop,              -- [std_logic]
            o_stream    => o_stream,            -- [AXI4_Streaming_32_t]
            i_stop1     => i_stop);             -- [std_logic]
 
    -- checker
         
    
    E_CHK_BUFFER: process (i_clk)
        variable chk_buffer_wp   : integer range chk_buffer'low to chk_buffer'high+1 := 0; -- write pointer
    begin
        chk_buffer_in_valid <= i_stream.valid;
        chk_buffer_in       <= i_stream;        
        if rising_edge(i_clk) then
            if chk_buffer_in_valid = '1' then
                chk_buffer(chk_buffer_wp) <= chk_buffer_in;
                chk_buffer_wp             := chk_buffer_wp + 1;
                if chk_buffer_wp = chk_buffer'high + 1 then
                    chk_buffer_wp := 0;
                end if;
            end if;
        end if;
    end process; 
 
    P_CHECK: process(i_clk)
        variable chk_buffer_rp      : integer range chk_buffer'low to chk_buffer'high+1 := 0; -- read pointer
        variable idx                : integer ;       
    begin
        if rising_edge (i_clk) then
            if o_stream.valid = '1' then
                chk_dut_out <= o_stream;
                idx         :=(chk_buffer_out.data'length);
                for i in o_stream.keep'length-1 downto 0 loop
                    if o_stream.keep(i) = '1' then
                        chk_buffer_out.data(idx-1 downto idx-i_stream.data'length)  <= chk_buffer(chk_buffer_rp).data;
                        --chk_buffer_ out. (valid & keep) are not used.  
                        chk_buffer_out.error                                        <= chk_buffer(chk_buffer_rp).error;
                        chk_buffer_out.last                                         <= chk_buffer(chk_buffer_rp).last;
                        chk_buffer_rp                                               := chk_buffer_rp + 1;
                        idx                                                         := idx - i_stream.data'length;
                    else
                        chk_buffer_out.data(idx-1 downto idx-i_stream.data'length)  <= "--------";
                        idx                                                         := idx-i_stream.data'length;
                    end if;
                    if chk_buffer_rp = chk_buffer'high + 1 then
                        chk_buffer_rp := 0;
                    end if;
                end loop;
                check_match(chk_buffer_out.data, chk_dut_out.data, " Data in not equals data out");
                check_equal(chk_buffer_out.error, chk_dut_out.error, "last data signal is not enabled");
                check_equal(chk_buffer_out.last, chk_dut_out.last, "error signal is not enabled");     
            else
                chk_dut_out     <= AXI4_Streaming_32_T_dontcare;
                chk_buffer_out  <= AXI4_Streaming_32_T_dontcare;                          
            end if;   
        end if;
    end process;
 
    i_stop <= '0';
    P_CHECK_STOP: process(i_clk)
    begin
        if rising_edge(i_clk) then
            check_equal(i_stop, o_stop, "Not expecting any reverse flow control.");
        end if;
    end process;
end architecture sim;

