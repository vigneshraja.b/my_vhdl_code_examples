-------------------------------------------------------------------------------
-- Title      : Test bench for design "AXI_wordise"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : AXI_wordise_pak_gen_th.vhd
-- Author     : Vignesh raja Balu <v.balu@massey.ac.nz>
-- Company    : Center for Research in Analog and VLSI micro-system Design lab, Massey University
-- Created    : 2018-07-24
-- Last update: 2018-07-28
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2018 Center for Research in Analog and VLSI micro-system Design lab, Massey University
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-02-22  1.0      vbalu   Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
library vunit_lib;
context vunit_lib.vunit_context;
context vunit_lib.com_context;
 
library osvvm;
use osvvm.RandomPkg.all;
 
library work;
use work.AXI4_pkg.all;
 
------------------------------------------------------------------------------------------------------------------------
 
entity AXI_wordise_pkt_gen_th is
    generic(
        
        g_PUBLISHER_NAME    : string;
        g_NUM_PKTS          : natural;
        g_RDM_PKT_WAIT      : natural);
    port(
        i_clk               : in  std_logic;
        i_clk_reset         : in  std_logic;
        o_stream_req        : out AXI4_Streaming_8_t);
end entity AXI_wordise_pkt_gen_th;

architecture sim of AXI_wordise_pkt_gen_th is
     
    signal stream       : AXI4_Streaming_8_t;
    signal eof_pkt_gen  : std_logic := '0';
    
    procedure pkt_gen ( rand_pkt_gap        : boolean;
                        rand_valid          : boolean;
                        signal clk          : std_logic;
                        signal stream       : out AXI4_Streaming_8_t;
                        signal eof_pkt_gen  : out std_logic) is
        variable pkt_len    : natural := 0;
        variable rand_wait  : RandomPType; 
    begin
        for pn in 1 to g_NUM_PKTS loop 
            pkt_len       := rand_wait.randInt(1, g_NUM_PKTS);
            stream.last   <= '0';
            stream.error  <= '0';
            for pl in 1 to pkt_len loop
                stream.keep   <= (others => '1');
                stream.data   <= std_logic_vector(to_unsigned(rand_wait.randInt(1, g_NUM_PKTS),stream.data'length)); -- random data value
                stream.valid  <= '1';
                if rand_valid then
                    if stream.data(rand_wait.randInt(0,stream.data'high)) = '1' then
                        stream.valid <= '0';
                    end if;
                end if;    
                wait until rising_edge(clk);
            end loop;
            stream.keep   <= (others => '1');
            stream.data   <= std_logic_vector(to_unsigned(rand_wait.randInt(1, g_NUM_PKTS),stream.data'length));
            stream.valid  <= '1';
            if rand_valid then
                if stream.data(rand_wait.randInt(0,stream.data'high)) = '1' then
                    stream.valid <= '0';
                end if;
            end if;
            stream.last   <= '1';
            stream.error  <= '1';
            wait until rising_edge(clk);                    
            if not (rand_pkt_gap)  then
                stream.valid  <= '0';
            else 
                stream.valid  <= '0';
                for rw in 1 to rand_wait.randInt(1,g_RDM_PKT_WAIT) loop
                    wait until rising_edge(clk);
                end loop;
            end if;
        end loop;
        eof_pkt_gen <= '1';
    end procedure;

begin -- architecture sim
  
    P_OUTPUT_PKTS: process
        variable self           : actor_t;
        variable message        : msg_t;
        variable reply_msg      : msg_t;
        variable rand_pkt_gap   : boolean;
        variable rand_valid     : boolean;
        variable rand_wait      : RandomPType;
    begin
        stream <= AXI4_STREAMING_8_T_ZERO; 
        wait until i_clk_reset = '0';     
        rand_wait.InitSeed(rand_wait'instance_name);
        wait until rising_edge(i_clk);        
        self    := find(g_PUBLISHER_NAME);
        receive(net, self, message);
        rand_pkt_gap    := pop(message);
        rand_valid      := pop(message);
        
        pkt_gen(rand_pkt_gap, rand_valid, i_clk, stream, eof_pkt_gen);
        reply_msg := new_msg;
        push(reply_msg, eof_pkt_gen);
        reply(net, message, reply_msg);
        
    end process;
o_stream_req <= stream;
end architecture sim;
        



